# browserslist-badges

> Generate badges from browserslist settings.

[![npm](https://img.shields.io/npm/v/browserslist-badges.svg?style=flat-square)](https://www.npmjs.com/package/browserslist-badges)
[![Standard Version](https://img.shields.io/badge/release-standard%20version-44aa44.svg?style=flat-square)](https://github.com/conventional-changelog/standard-version)
[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://raw.githubusercontent.com/pixelass/browserslist-badges/master/LICENSE)
[![GitHub issues](https://img.shields.io/github/issues/pixelass/browserslist-badges.svg?style=flat-square)](https://github.com/pixelass/browserslist-badges/issues)

[![Travis branch](https://img.shields.io/travis/pixelass/browserslist-badges/master.svg?style=flat-square)](https://travis-ci.org/pixelass/browserslist-badges)
[![bitHound](https://img.shields.io/bithound/code/github/pixelass/browserslist-badges.svg?style=flat-square)](https://www.bithound.io/github/pixelass/browserslist-badges)
[![bitHound](https://img.shields.io/bithound/devDependencies/github/pixelass/browserslist-badges.svg?style=flat-square)](https://www.bithound.io/github/pixelass/browserslist-badges)
[![Coveralls](https://img.shields.io/coveralls/pixelass/browserslist-badges.svg?style=flat-square)](https://coveralls.io/github/pixelass/pixelass/browserslist-badges)
[![Inline docs](http://inch-ci.org/github/pixelass/browserslist-badges.svg?branch=master)](http://inch-ci.org/github/pixelass/browserslist-badges)

[![Browserify](https://img.shields.io/badge/build-browserify-3c6991.svg?style=flat-square)](http://browserify.org/)
[![Babel](https://img.shields.io/badge/babel-stage--2-f5da55.svg?style=flat-square)](http://babeljs.io/docs/plugins/preset-stage-2/)
[![Babel](https://img.shields.io/badge/babel-transform--runtime-f5da55.svg?style=flat-square)](http://babeljs.io/docs/plugins/transform-runtime/)
[![code style xo](https://img.shields.io/badge/code_style-XO-64d8c7.svg?style=flat-square)](https://github.com/sindresorhus/xo)
[![test ava](https://img.shields.io/badge/test-🚀_AVA-0e1d5c.svg?style=flat-square)](https://github.com/avajs/ava)

[![yarn](https://img.shields.io/badge/yarn-friendly-2c8ebb.svg?style=flat-square)](https://yarnpkg.com/)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-44aa44.svg?style=flat-square)](http://commitizen.github.io/cz-cli/)
[![CSS modules](https://img.shields.io/badge/css-modules-44aa44.svg?style=flat-square)](https://github.com/css-modules/css-modulesify)
[![CSS next](https://img.shields.io/badge/css-next-44aa44.svg?style=flat-square)](http://cssnext.io/)

<!-- toc -->

- [Developing](#developing)

<!-- tocstop -->

## Examples

![Chrome](https://rawgit.com/pixelass/browserslist-badges/master/build/Chrome.svg)
![Firefox](https://rawgit.com/pixelass/browserslist-badges/master/build/Firefox.svg)
![Safari](https://rawgit.com/pixelass/browserslist-badges/master/build/Safari.svg)
![Edge](https://rawgit.com/pixelass/browserslist-badges/master/build/Edge.svg)

## Developing

To start a dev server and start developing try the following commands

* `start`: starts the dev server and builds the required files
* `test`: runs test and lints files
* `dev`: starts the dev server and watches the required files
* `babel`: generates lib from source
* `build`: builds all files from source
* `watch`: builds and watches all files from source
* `lint`: lints JavaScript files
* `release`: release new version using "standard-version"

© 2017 by [Gregor Adams](http://pixelass.com)
