/* global document */
import createBadges, {createShieldsBadge} from '../src/badges'
import './main.css' // eslint-disable-line import/no-unassigned-import

const app = document.getElementById('app')

const badgeData = {
  chrome: ['last 2 versions'],
  firefox: ['last 2 versions', 'lte'],
  ie: [9, 10, 11],
  opera: [42, 43],
  edge: [13, 14]
}

const browserslist = [
  'last 2 Chrome versions',
  '> 5%',
  'Firefox > 20',
  'Chrome >= 20',
  'ie 6-8',
  'opera <= 12',
  'iOS 7'
]

Object.keys(badgeData).forEach(browser => {
  const versions = badgeData[browser]
  const div = document.createElement('div')
  div.innerHTML = createShieldsBadge({}, browser, ...versions)
  app.appendChild(div)
})

createBadges(browserslist).forEach(browser => {
  const div = document.createElement('div')
  const img = document.createElement('img')
  img.src = browser.badge
  div.appendChild(img)
  app.appendChild(div)
})
