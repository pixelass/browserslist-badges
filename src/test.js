import {browserslist} from '../package.json' // eslint-disable-line import/extensions
import createProjectBadges from './'

const testList = [
  'last 2 Chrome versions',
  '> 5%',
  'Firefox > 20',
  'Edge >= 14',
  'ie 6-8',
  'Opera <= 12',
  'iOS 7'
]

createProjectBadges(browserslist, {dir: '../build'})
  .then(() =>
    createProjectBadges(testList, {dir: '../build'})
      .catch(err => {
        throw err
      })).catch(err => {
        throw err
      })
