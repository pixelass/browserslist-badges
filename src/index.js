import path from 'path'
import log from 'winston'
import mkdirp from 'mkdirp'
import createBadges from './badges'
import download from './badges/download'

const downloadBadges = (badges, outFolder) => Promise.all(badges.map(({name, badge}) =>
  download({url: badge, outFile: path.join(outFolder, `./${name}.svg`)})
    .then(fileName => {
      log.info(`Wrote file ${fileName}`)
      Promise.resolve(fileName)
    })
    .catch(err => {
      Promise.reject(err)
    })
))

const buildRequest = (browserslist, options = {}) => new Promise((resolve, reject) => {
  const settings = {
    dir: './badges',
    ...options
  }
  const outFolder = path.join(__dirname, settings.dir)
  const badges = createBadges(browserslist)

  mkdirp(outFolder, err => {
    if (err) {
      reject(err)
    }
    resolve({badges, outFolder})
  })
})

const createProjectBadges = (browserslist, options = {}) =>
  buildRequest(browserslist, options)
  .then(({badges, outFolder}) =>
    downloadBadges(badges, outFolder)
  )
  .catch(err => Promise.reject(err))

export default createProjectBadges
