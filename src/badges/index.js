const transformString = str => str.replace(/-/g, '--').replace(/_/g, '__').replace(/\s+/g, '%20')

const img = (content, options) => {
  const settings = {
    type: 'markdown',
    ...options
  }
  const label = transformString(content.label)
  const status = transformString(content.status)
  const url = `https://img.shields.io/badge/${label}-${status}-44aa44.svg?style=flat-square`
  if (settings.type === 'markdown') {
    return `[${content.label}](${url})`
  } else if (settings.type === 'img-tag') {
    return `<img src="${url}" alt="${content.label}"/>`
  } else if (settings.type === 'url') {
    return url
  }
}

const createShieldsBadge = (options, browser, ...versions) => {
  const badge = img({
    label: browser,
    status: versions.join(', ')
  }, options)
  return badge
}

const createBadges = browserslist => {
  const pattern = new RegExp('chromeandroid |chrome |safari |opera |firefox |safari |edge |ios |ie ', 'i')
  const waste = browserslist.map(x => x.split(pattern))
  const browsers = browserslist.map((x, i) => {
    waste[i].forEach(y => {
      x = x.replace(y, '')
    })
    return {
      name: (x || 'All').trim(),
      status: waste[i].join(' ')
    }
  })
  return browsers.map(b => ({name: b.name, badge: createShieldsBadge({type: 'url'}, b.name, b.status)}))
}

export {createShieldsBadge}

export default createBadges
