import fs from 'fs'
import request from 'request'

const encoding = 'binary'

const download = ({url, outFile}) =>
  new Promise((resolve, reject) => {
    // 10000ms timeout
    setTimeout(() => {
      reject(new Error(`${url}: timeout after 10000ms`))
    }, 10000)
    request.get(url, encoding, (err, res) => {
      if (err) {
        reject(err)
      } else if (res.statusCode === 200) {
        fs.writeFile(outFile, res.body, {encoding}, err => {
          if (err) {
            reject(err)
          }
          resolve(outFile)
        })
      } else {
        reject(res)
      }
    })
  })

export default download
