import express from 'express'
import path from 'path'
import {config, name, description} from '../package.json'

const serve = () => {
  const PORT = process.env.PORT || config.devPort
  const app = express()
  app.use(express.static('docs'))
  app.set('view engine', 'pug')
  app.get('/', function (req, res) {
    res.render('index', {
      name,
      description
    })
  })
  app.listen(PORT, function () {
    console.log(`Example app listening on port ${PORT}!`)
  })
}

export default serve
